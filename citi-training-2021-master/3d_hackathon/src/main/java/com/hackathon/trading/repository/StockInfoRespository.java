package com.hackathon.trading.repository;

import com.hackathon.trading.entity.StockInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StockInfoRespository extends JpaRepository<StockInfo, Long> {
    public List<StockInfo> findByStockTicker(String stockTicker);
}
