package com.hackathon.trading.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
public class StockInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String stockTicker;
    private double price;
    private Long volume;
    private String buyOrSell;
    private int statusCode;
    @Temporal(TemporalType.TIMESTAMP)
    private Date timeStamp = new Date(System.currentTimeMillis());

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStockTicker() {
        return stockTicker;
    }

    public void setStockTicker(String stockTicker) throws Exception {
        if(stockTicker.length() > 6 || stockTicker.length() < 1){
            throw new Exception("Ticker name must be between 1 and 6");
        }
        else{
            this.stockTicker = stockTicker;
        }
    }

    public double getPrice() {
        return price;
    }

    public void setPrice (double price) throws Exception {
        if(price <= 0){
            throw new Exception("Price must be positive");
        }
        else{
            this.price = price;
        }
    }

    public Long getVolume() {
        return volume;
    }

    public void setVolume(Long volume) throws Exception{
        if(volume <= 0){
            throw new Exception("Volume must be positive");
        }
        else{
            this.volume = volume;
        }
    }

    public String getBuyOrSell() {
        return buyOrSell;
    }

    public void setBuyOrSell(String buyOrSell) {
        this.buyOrSell = buyOrSell;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    @Override
    public String toString() {
        return "StockInfo{" +
                "id=" + id +
                ", stockTicker='" + stockTicker + '\'' +
                ", price=" + price +
                ", volume=" + volume +
                ", buyOrSell='" + buyOrSell + '\'' +
                ", statusCode=" + statusCode +
                '}';
    }
}
