package com.hackathon.trading.controller;

import com.hackathon.trading.entity.StockInfo;
import com.hackathon.trading.service.StockInfoService;
import io.swagger.models.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/api/v1/stocks")
public class StockInfoController {

    private static final Logger LOG = LoggerFactory.getLogger(StockInfoController.class);

    @Autowired
    StockInfoService stockInfoService;

    @GetMapping
    public  ResponseEntity<List<StockInfo>>findAll() {
        try {
            return new ResponseEntity<List<StockInfo>>(stockInfoService.findAll(), HttpStatus.OK);
        } catch(NoSuchElementException ex) {
            // return 404
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("{id}")
    public ResponseEntity<StockInfo> findById(@PathVariable long id) {
        try {
            return new ResponseEntity<StockInfo>(stockInfoService.findById(id), HttpStatus.OK);
        } catch(NoSuchElementException ex) {
            // return 404
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public ResponseEntity<StockInfo> create(@RequestBody StockInfo stockInfo) {
        LOG.info("Request to create a new stock order [" + stockInfo + "]");
        return new ResponseEntity<StockInfo>(stockInfoService.save(stockInfo), HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<StockInfo> update(@RequestBody StockInfo stockInfo){
        try {
            LOG.info("Request to update a stock order [" + stockInfo + "]");
            return new ResponseEntity<StockInfo>(stockInfoService.update(stockInfo), HttpStatus.OK);
        } catch(NoSuchElementException ex) {
            return new ResponseEntity<StockInfo>(HttpStatus.NOT_FOUND);
        }
    }

    //Delete mapping
    @DeleteMapping("/{id}")
    public ResponseEntity<StockInfo> delete(@PathVariable long id){
        try{
            stockInfoService.delete(id);
        } catch(EmptyResultDataAccessException ex){
            return new ResponseEntity<StockInfo>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<StockInfo>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/ticker/{stockTicker}")
    public ResponseEntity<List<StockInfo>> findByStockTicker(@PathVariable String stockTicker) {
        try {
            return new ResponseEntity<List<StockInfo>>(stockInfoService.findByStockTicker(stockTicker), HttpStatus.OK);
        } catch(NoSuchElementException ex) {
            // return 404
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

}
