package com.hackathon.trading.service;

import com.hackathon.trading.entity.StockInfo;
import com.hackathon.trading.repository.StockInfoRespository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

@Service
public class StockInfoService {

    @Autowired
    StockInfoRespository stockInfoRespository;

    public List<StockInfo> findAll(){
        return this.stockInfoRespository.findAll();
    }

    public StockInfo findById(long id) {
        return stockInfoRespository.findById(id).get();
    }

    public StockInfo save(StockInfo stockInfo){
        return this.stockInfoRespository.save(stockInfo);
    }

    public StockInfo update(StockInfo stockInfo) {
        stockInfoRespository.findById(stockInfo.getId()).get();

        return stockInfoRespository.save(stockInfo);
    }

    public void delete(long id){
        stockInfoRespository.deleteById(id);
    }


    public List<StockInfo> findByStockTicker(String stockTicker){
        return stockInfoRespository.findByStockTicker(stockTicker);
    }

}
