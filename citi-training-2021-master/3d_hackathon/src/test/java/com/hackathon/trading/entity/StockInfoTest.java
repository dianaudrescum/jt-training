package com.hackathon.trading.entity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.assertj.core.api.Fail.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class StockInfoTest {

//    TODO: finish entity tests

    private static long testVolume = 1600;
    private static String testBuyOrSell = "Buy";
    private static int testStatusCode = 0;

    private StockInfo stockInfoTest;

    @BeforeEach
    public void setup() {
        this.stockInfoTest = new StockInfo();
    }

    @Test
    public void setStockTicker() throws Exception{
        final String testStockTicker = "C";

        this.stockInfoTest.setStockTicker(testStockTicker);

        assertEquals(testStockTicker, this.stockInfoTest.getStockTicker());
    }

    @Test
    public void wrongLengthTickersAreRejected() throws Exception {
        final String testStockTicker = "toolongforticker";
        try{
            this.stockInfoTest.setStockTicker(testStockTicker);
            fail("Exception not thrown!");

        } catch(Exception e){
            assertEquals("Ticker name must be between 1 and 6", e.getMessage());
        }

    }

    @ParameterizedTest
    @ValueSource(doubles = {0,-5, -4840, -239, -111.323, -0.001, -0.0, -9999.99})
    public void nonPositivePriceIsRejected(double price) throws Exception {
        try{
            this.stockInfoTest.setPrice(price);
            fail("Exception not thrown!");

        } catch(Exception e){
            assertEquals("Price must be positive", e.getMessage());
        }
    }

    @ParameterizedTest
    @ValueSource(longs = {0,-5, -4840, -239, -111, -9999})
    public void nonPositiveVolumeIsRejected(long volume) throws Exception {
        try{
            this.stockInfoTest.setVolume(volume);
            fail("Exception not thrown!");

        } catch(Exception e){
            assertEquals("Volume must be positive", e.getMessage());
        }
    }


}
