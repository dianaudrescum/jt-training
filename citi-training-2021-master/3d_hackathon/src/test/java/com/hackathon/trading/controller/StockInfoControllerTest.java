package com.hackathon.trading.controller;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@SpringBootTest
@AutoConfigureMockMvc
public class StockInfoControllerTest {

    //TODO: finish adding tests for the controller

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void stocksEndpointIsOk() throws Exception {
        MvcResult mvcResult = this.mockMvc.perform(get("/api/v1/stocks"))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();
    }

    @ParameterizedTest
    @ValueSource(longs = {5, 7, 3, 6})
    public void stockIdIsFound(long id) throws Exception {
        MvcResult mvcResult = this.mockMvc.perform(get("/api/v1/stocks/" + id))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();
    }

}