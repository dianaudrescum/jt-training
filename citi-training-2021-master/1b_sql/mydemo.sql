CREATE DATABASE DemoDB;
USE DemoDB;
SHOW databases;

CREATE TABLE Employees (
	Name VARCHAR(20),
    Position VARCHAR(25),
    Salary INT
);

SHOW TABLES;

DROP TABLE Employees;

CREATE TABLE Employees (
	ContactID INT auto_increment primary key,
	Name VARCHAR(30) NOT NULL,
    Position VARCHAR(25),
    Salary INT
);

CREATE TABLE Contact (
	ContactID INT auto_increment,
    LastName VARCHAR(50) NOT NULL,
    FirstName VARCHAR(50),
    Address VARCHAR(50),
    City VARCHAR(50),
	constraint pk_contact PRIMARY KEY (ContactID) -- alternative way to set primary key
);

ALTER TABLE Contact MODIFY LastName VARCHAR(30);
ALTER TABLE Contact DROP City;
ALTER TABLE Contact DROP primary key; 
ALTER TABLE Contact DROP primary key, MODIFY ContactID int;

DROP TABLE Contact;
DROP TABLE country;

CREATE TABLE Country (
	CountryID INT AUTO_INCREMENT PRIMARY KEY,
    CountryName VARCHAR(50),
    Continent VARCHAR(20),
    Capital VARCHAR (50)
);

CREATE TABLE Contact (
	ContactID INT AUTO_INCREMENT,
    LastName VARCHAR(50) NOT NULL,
    FirstName VARCHAR(50),
    Address VARCHAR(50),
    City VARCHAR(50),
    CountryID INT,
    constraint pk_contact PRIMARY KEY (ContactID),constraint pk_contact PRIMARY KEY (ContactID),
    foreign key (CountryID) REFERENCES Country(CountryID)
);

ALTER TABLE Contact ADD Email VARCHAR(50) UNIQUE NOT NULL;

SHOW COLUMNS FROM Contact;

INSERT INTO Country (CountryName, Continent, Capital) VALUES
(2,'Australia', 'Oceania', 'Canberra');

SELECT * FROM Country;

INSERT INTO Country VALUES (2,"Ireland","Europe","Dublin");

INSERT INTO Contact(FirstName,LastName,Email,CountryID) VALUES
('Darryl','Greig','dsgreig@gmail.com',1);

SELECT * FROM Contact;

SELECT * FROM Country;
INSERT INTO Country VALUES (0,"England","Europe","London");

DELETE FROM Country WHERE CountryID=1;
SELECT * FROM Country;

delete from Contact where CountryID=1;
select * from Contact;

Select Name, Continent From world.country;
insert into Country(CountryName, Continent) select Name, Continent FROM world.country;