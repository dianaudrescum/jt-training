USE Northwind;
select * from shippers;
insert into Shippers (CompanyName,Phone) Values ('AllIrish Stout','2345689');
delete from Shippers where ShipperID=7;
alter table Shippers AUTO_INCREMENT=4;

update shippers set phone='(234)57789' where shipperid=7;
update shippers set phone='(234)57789' where CompanyName='AllIrish Stout';

select * from orders;
delete from orders where year(OrderDate)=2017; -- violates constraints

select ProductName, UnitPrice from Products where categoryid=5;

UPDATE Products set UnitPrice = 1.1*(UnitPrice) where CategoryID=5;
select * from products where categoryid=5;

select * from suppliers;

select CompanyName as Suppliers, ContactName Contact, Country, City from Suppliers 
where country <> 'UK';

select OrderID, OrderDate from Orders where OrderDate < '2017-01-01' and orderdate >= '2016-09-09';

select OrderID, OrderDate from Orders where OrderDate between '2016-09-01' and '2016-10-01';

select OrderID, OrderDate from Orders where YEAR(OrderDate) = 2016 and MONTH(OrderDate)=9;

select CompanyName from Customers where CompanyName like '%Market%';

select CustomerId, CompanyName from Customers where CustomerID like '_N__';

select CustomerID, CompanyName from Customers where CustomerID like '_N___';

select CompanyName, Region from Suppliers where Region IS null;
select ProductName, UnitPrice, CategoryID from Products order by CategoryID, UnitPrice DESC;
select distinct Country, City from Customers order by Country;

select count(*) from Customers;

select CategoryName,
	CASE CategoryName
		when 'Beverages' then 'Drinks'
		when 'Condiments' then 'Seasonings'
        when 'Confections' then 'Sweets'
        ELSE 'Something Else'
	END COMMENT
from Categories;

select ProductName, UnitPrice,
	CASE
		when UnitPrice > 40 then 'Expensive'
        when UnitPrice > 20 then 'Reasonable'
        ELSE 'Cheap'
	END 'Value for money'
from Products;

select * from categories;
update categories set categoryname='Drinks' where categoryname='beverages';

select * from categories where CategoryID!=1 and CategoryName like 'C%';