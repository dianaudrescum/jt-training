public class EnumTest {
    public static void main(String[] args) {
        Account account = new Account("JT", 5, Currency.EUR);

        System.out.println("Balance: " + account.getBalance() + " Currency " + account.getCurrency());
    }
}
