import java.util.HashSet;
import java.util.Iterator;

public class CollectionsTest {
    public static void main(String[] args) {
        HashSet<Account> accounts;
        accounts = new HashSet<Account>();

        double[] amounts = {5,10,15,20,25};
        String[] names = {"a", "b", "c", "d", "e"};

        Account current;
        for (int i = 0; i < amounts.length; ++i) {
            current = new Account();
            current.setAccountName(names[i]);
            current.setBalance(amounts[i]);
            accounts.add(current);
        }

        Iterator<Account> iter = accounts.iterator();

//        while (iter.hasNext()) {
//            Account nextAccount = new Account();
//            System.out.println("Name: " + nextAccount.getAccountName());
//            System.out.println("Balance: " + nextAccount.getBalance());
//            nextAccount.addInterest();
//            System.out.println("New Balance: " + nextAccount.getBalance());
//        }

        for (Account nextAccount: accounts) {
            System.out.println("Name: " + nextAccount.getAccountName());
            System.out.println("Balance: " + nextAccount.getBalance());
            nextAccount.addInterest();
            System.out.println("New Balance: " + nextAccount.getBalance());
        }

    }
}
