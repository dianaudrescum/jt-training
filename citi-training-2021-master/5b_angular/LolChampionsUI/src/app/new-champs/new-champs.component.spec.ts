import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewChampsComponent } from './new-champs.component';

describe('NewChampsComponent', () => {
  let component: NewChampsComponent;
  let fixture: ComponentFixture<NewChampsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewChampsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewChampsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
