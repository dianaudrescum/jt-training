import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditChampsComponent } from './edit-champs.component';

describe('EditChampsComponent', () => {
  let component: EditChampsComponent;
  let fixture: ComponentFixture<EditChampsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditChampsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditChampsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
