import { Component, OnInit } from '@angular/core';
import { RestApiService } from "../shared/rest-api.service";

@Component({
  selector: 'app-list-champs',
  templateUrl: './list-champs.component.html',
  styleUrls: ['./list-champs.component.css']
})
export class ListChampsComponent implements OnInit {
  LolChampions: any = [];
  constructor(
    public restApi: RestApiService;
  ) { }

  ngOnInit(): void {
   this.
  }

  loadLolChampions(){
    return this.restApi.getLolChampions().subscribe((data: {}) => {
      this.LolChampions = data;
    })
  }

  deleteLolChampions(id:any) {
    if (window.confirm('r u sure?'))
  }
}

import { Component, OnInit } from '@angular/core';
import { RestApiService } from "../shared/rest-api.service";
import { ThrowStmt } from '@angular/compiler';

@Component({
  selector: 'app-shipper-list',
  templateUrl: './shipper-list.component.html',
  styleUrls: ['./shipper-list.component.css']
})
export class ShipperListComponent implements OnInit {

  Shippers: any = [];
  constructor(
    public restApi: RestApiService
    ) { }

  ngOnInit(): void {
    this.loadShippers()
  }

  loadShippers() {
    return this.restApi.getShippers().subscribe((data: {}) => {
        this.Shippers = data;
    })
  }

  deleteShipper(id:any) {
    if (window.confirm('Are you sure, you want to delete?')){
      this.restApi.deleteShipper(id).subscribe(data => {
        this.loadShippers()
      })
    }
  }

}
