import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EditChampsComponent } from './edit-champs/edit-champs.component';
import { ListChampsComponent } from './list-champs/list-champs.component';
import { NewChampsComponent } from './new-champs/new-champs.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'list-champs' },
  { path: 'new-champs', component: NewChampsComponent},
  { path: 'list-champs', component: ListChampsComponent},
  { path: 'edit-champs', component: EditChampsComponent},
  { path: '**', component: ListChampsComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
