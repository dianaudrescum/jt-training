var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
// This is a parameterized class decorator.
// A parameterized class decorator acts as a factory. It receives arguments from an applied decorator and returns the function to execute, to do the decoration work. 
function SecureParameterized(encrypt, digest) {
    console.log("SecureParameterized decorator factory called.");
    return function (ctor) {
        console.log("SecureParameterized decorator called.");
        ctor.prototype.encrypt = encrypt;
        ctor.prototype.digest = digest;
    };
}
let Passport = class Passport {
    constructor(name, number) {
        console.log("Passport constructor called.");
        this.name = name;
        this.number = number;
    }
};
Passport = __decorate([
    SecureParameterized(true, true)
], Passport);
let Basket = class Basket {
    constructor(name, value) {
        console.log("Basket constructor called.");
        this.name = name;
        this.value = value;
    }
};
Basket = __decorate([
    SecureParameterized(false, true)
], Basket);
// Client code.
let pp1 = new Passport("Mary", "123456789");
console.log(pp1);
console.log(`pp1 encrypt? ${pp1['encrypt']}`);
console.log(`pp1 digest? ${pp1['digest']}`);
let pp2 = new Passport("Joe", "987654321");
console.log(pp2);
console.log(`pp2 encrypt? ${pp2['encrypt']}`);
console.log(`pp2 digest? ${pp2['digest']}`);
let b1 = new Basket("Emily", 123.45);
console.log(b1);
console.log(`b1 encrypt? ${b1['encrypt']}`);
console.log(`b1 digest? ${b1['digest']}`);
let b2 = new Basket("Tom", 543.21);
console.log(b2);
console.log(`b2 encrypt? ${b2['encrypt']}`);
console.log(`b2 digest? ${b2['digest']}`);
