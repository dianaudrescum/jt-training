var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
// This is a parameterized class decorator that extends the original ctor/class. 
// It's similar to the previous example, except that you can pass parameters into the decorator when you apply it.
function TimestampedInstanceParameterized(date, time) {
    console.log("TimestampedInstanceParameterized decorator factory called.");
    return function (ctor) {
        console.log("TimestampedInstanceParameterized decorator called.");
        return class extends ctor {
            constructor() {
                super(...arguments);
                this.tsDate = date ? new Date().getDate() : "[n/a]";
                this.tsTime = time ? new Date().getSeconds() : "[n/a]";
            }
        };
    };
}
let Buzzer = class Buzzer {
    constructor(what) {
        console.log("Buzzer constructor called.");
        this.what = what;
    }
};
Buzzer = __decorate([
    TimestampedInstanceParameterized(false, true)
], Buzzer);
// Client code.
let bz1 = new Buzzer("Coffeetime");
console.log(bz1);
console.log(`bz1 tsDate: ${bz1['tsDate']}`);
console.log(`bz1 tsTime: ${bz1['tsTime']}`);
setTimeout(() => {
    let bz2 = new Buzzer("Coffeetime again!");
    console.log(bz2);
    console.log(`bz2 tsDate: ${bz2['tsDate']}`);
    console.log(`bz2 tsTime: ${bz2['tsTime']}`);
}, 5000);
