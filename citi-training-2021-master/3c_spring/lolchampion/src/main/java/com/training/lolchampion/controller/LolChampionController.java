package com.training.lolchampion.controller;

import com.training.lolchampion.entities.LolChampion;
import com.training.lolchampion.service.LolChampionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/lolchampion")
public class LolChampionController {
    @Autowired
    private LolChampionService lolChampionService;

    @GetMapping
    public List<LolChampion> findall() {
        return lolChampionService.findAll();
    }

    @PostMapping
    public LolChampion save(@RequestBody LolChampion lolChampion) {
        return this.lolChampionService.save(lolChampion);
    }

}