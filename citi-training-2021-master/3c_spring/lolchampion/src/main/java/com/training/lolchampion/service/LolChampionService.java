package com.training.lolchampion.service;

import com.training.lolchampion.entities.LolChampion;
import com.training.lolchampion.repository.LolChampionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class LolChampionService {


    @Autowired
    LolChampionRepository lolChampionRepository;

    //method to retrieve all LolChampion objects
    public List<LolChampion> findAll(){
        return lolChampionRepository.findAll();
    }

    //method to save a LolChampion object
    public LolChampion save(LolChampion lolChampion){
        return lolChampion;
    }

}
