package com.training.northwind.entities;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ShipperTest {

    private Shipper shipper;
    @BeforeEach
    public void setup() {
        this.shipper = new Shipper();
    }

    @Test
    public void setCompanyNameTest() {
        this.shipper.setCompanyName("JUnit Test Company");

        assertEquals(this.shipper.getCompanyName(), "JUnit Test Company");
    }

    @Test
    public void shipperSanityCheckTest() {
        Shipper shipper = new Shipper();
        shipper.setPhone("23423424224242");
        String testNum = "23423424224242";

        assert(shipper.getPhone().equals(testNum));
    }
}
