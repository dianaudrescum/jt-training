package com.training.northwind.repository;

import com.training.northwind.entities.Shipper;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface ShipperRepository extends JpaRepository<Shipper, Long> {
    List<Shipper> findShipperByPhone(String phone);
}
