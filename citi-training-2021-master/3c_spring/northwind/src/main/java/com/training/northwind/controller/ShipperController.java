package com.training.northwind.controller;

import com.training.northwind.entities.Shipper;
import com.training.northwind.service.ShipperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/v1/shipper")
public class  ShipperController {

    @Autowired
    private ShipperService shipperService;

    @GetMapping
    public List<Shipper> findAll() {
        return shipperService.findAll();
    }


    @GetMapping("/{id}")
    public ResponseEntity<Shipper> findByID(@PathVariable long id) {
        try {
            return new ResponseEntity<Shipper>(shipperService.findById(id), HttpStatus.OK);
        } catch (NoSuchElementException ex){
            //return 404
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public ResponseEntity<Shipper> create(Shipper shipper) {
        return new ResponseEntity<Shipper>(shipperService.save(shipper), HttpStatus.CREATED);

    }
}
