package com.training.springdemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringDemoApplication implements ApplicationRunner {

    @Autowired
    private Person person;

    public static void main(String[] args) {
        SpringApplication.run(SpringDemoApplication.class, args);
    }

    public void run(ApplicationArguments args) throws Exception {
        System.out.println("Spring has sprung!");
        person.sayHello();
        person.feedPet();
    }

}


