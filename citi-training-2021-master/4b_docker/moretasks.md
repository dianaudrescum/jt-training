# More docker tasks

[Dev Basics](https://training.play-with-docker.com/dev-stage1/)

[Ops Basics](https://training.play-with-docker.com/ops-stage1/)

[Deploy container recap](https://www.katacoda.com/courses/docker/deploying-first-container)

[Dockerfile recap](https://www.katacoda.com/courses/docker/create-nginx-static-web-server)

[All docker labs](https://www.katacoda.com/courses/docker)