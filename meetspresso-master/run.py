from meetspresso import app

# program entry point
if __name__ == '__main__':
    app.run(debug=False,host='127.0.0.1',port=8000)
