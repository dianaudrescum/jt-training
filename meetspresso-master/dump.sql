CREATE DATABASE  IF NOT EXISTS `meetspresso` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */;
USE `meetspresso`;
-- MySQL dump 10.13  Distrib 8.0.21, for macos10.15 (x86_64)
--
-- Host: 127.0.0.1    Database: meetspresso
-- ------------------------------------------------------
-- Server version	8.0.12

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `appointments`
--

DROP TABLE IF EXISTS `appointments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `appointments` (
  `appointmentid` int(11) NOT NULL AUTO_INCREMENT,
  `userid1` int(11) NOT NULL,
  `userid2` int(11) NOT NULL,
  `startdate` datetime NOT NULL,
  `enddate` datetime NOT NULL,
  `timezone` varchar(10) NOT NULL,
  `location` varchar(50) DEFAULT NULL,
  `status` varchar(20) NOT NULL,
  PRIMARY KEY (`appointmentid`),
  UNIQUE KEY `appointmentid_UNIQUE` (`appointmentid`),
  KEY `userid2_idx` (`userid2`),
  KEY `userid1_idx` (`userid1`),
  CONSTRAINT `userid1` FOREIGN KEY (`userid1`) REFERENCES `users` (`userid`),
  CONSTRAINT `userid2` FOREIGN KEY (`userid2`) REFERENCES `users` (`userid`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `appointments`
--

LOCK TABLES `appointments` WRITE;
/*!40000 ALTER TABLE `appointments` DISABLE KEYS */;
INSERT INTO `appointments` VALUES (1,11,12,'2020-08-07 15:30:00','2020-08-07 16:00:00','UTC+0','1234567890@vmr.citi.com','accepted'),(2,13,11,'2020-08-10 14:00:00','2020-08-10 15:00:00','UTC+0','2345678901@vmr.citi.com','accepted'),(3,20,17,'2020-08-07 10:00:00','2020-08-07 10:30:00','UTC+0','3456789012@vmr.citi.com\r','accepted'),(4,12,16,'2020-08-07 14:00:00','2020-08-07 14:30:00','UTC+0','4567890123@vmr.citi.com','accepted'),(5,13,19,'2020-08-07 12:00:00','2020-08-07 12:30:00','UTC+0','5678901234@vmr.citi.com','declined'),(6,14,18,'2020-08-10 09:00:00','2020-08-10 09:30:00','UTC+0','6789012345@vmr.citi.com','declined'),(7,17,19,'2020-08-10 11:30:00','2020-08-10 12:00:00','UTC+0','7890123456@vmr.citi.com','declined'),(8,11,20,'2020-08-07 13:00:00','2020-08-07 13:30:00','UTC+0','8901234567@vmr.citi.com','accepted'),(10,12,17,'2020-08-10 17:00:00','2020-08-10 17:30:00','UTC+0','9012345678@vmr.citi.com','accepted'),(31,12,11,'2020-01-01 00:01:00','2020-01-01 00:10:00','UTC+0','123456789@vmr.citi.com','declined'),(32,14,11,'2020-08-08 09:00:00','2020-08-08 09:30:00','UTC+0','Zoom Meeting Room: 123 456 7890 Password: 123456','pending');
/*!40000 ALTER TABLE `appointments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `random`
--

DROP TABLE IF EXISTS `random`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `random` (
  `randomid` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  PRIMARY KEY (`randomid`),
  UNIQUE KEY `randomid_UNIQUE` (`randomid`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `random`
--

LOCK TABLES `random` WRITE;
/*!40000 ALTER TABLE `random` DISABLE KEYS */;
INSERT INTO `random` VALUES (2,11),(6,16),(7,17),(8,18),(9,19),(10,20);
/*!40000 ALTER TABLE `random` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `userid` int(11) NOT NULL AUTO_INCREMENT,
  `soeid` varchar(7) NOT NULL,
  `password` varchar(45) NOT NULL,
  `salt` varchar(45) NOT NULL,
  `hashed` varchar(200) NOT NULL,
  `name` varchar(45) NOT NULL,
  `title` varchar(45) NOT NULL,
  `team` varchar(45) NOT NULL,
  PRIMARY KEY (`userid`),
  UNIQUE KEY `userid_UNIQUE` (`userid`),
  UNIQUE KEY `soeid_UNIQUE` (`soeid`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'ZZ99999','password','password','password','a','a','a'),(2,'ZZ99998','password','password','password','a','a','a'),(3,'ZZ99997','password','password','password','a','a','a'),(4,'ZZ99996','password','password','password','a','a','a'),(5,'ZZ99995','password','password','password','a','a','a'),(6,'ZZ99994','password','password','password','a','a','a'),(7,'ZZ99993','password','password','password','a','a','a'),(8,'ZZ99992','password','password','password','a','a','a'),(9,'ZZ99991','password','password','password','a','a','a'),(10,'ZZ99990','password','password','password','a','a','a'),(11,'AA12345','password','JWER6386BG','30972af06490b89a069bcd4e396dd4266432a643929d2e7a73e5fcb39dbbae99e7a84c3b3ed8db6a7263c4059551601d964ea86ef1299973dddd87175e711465','Andrew Adams','Vice President','Credit Quantiative Analysis'),(12,'JT49863','password','4VAHHOU4GP','e39f94f2767d149a5c2c3f47b8389cb8e4eca31cdbfb08d60959e8bb627e58ae330868c8b4b292a7bcbb7a3c480318edd3b94d44494e6dfb21121068c3ba8b09','Jonathan Tang','Summer Analyst','Technology'),(13,'MU49913','password','8YD9EL2LG5','502896961d7c6407fd0075e8e0322da0ba77dfe69fcc43e0ab6cf782a071a05a779ec56d84a26b23913bd76573aaa8874acbd75aec27627976a6f753098c7816','Diana Udrescu','Summer Analyst','Technology'),(14,'MD49859','password','I4NECA1OFK','92969d333625d8d5b5f47c52c63ac08522778acd528e0fbe587f467c2ffd9664cf912f5cdd9d3496af45c7fbb7c82d97bdd10ea66096f6d8ea194a8b5b1df21c','Madalina Dumitrescu','Summer Analyst','Technology'),(15,'MC12345','password','5VYHIH35NV','f3bdae7a673982737bfe6259c677a0d47ea9b63bfa5642efc985482ec09321756fd9a7580380706fd3214c5dbfd9edc51677f01181e9d8f15604820716cb7cc0','Michael Corbat','Managing Director','CEO'),(16,'AR12345','password','0NUB8CT04Q','7bdaa166c3dfc3210edb035b3144d016447ab4c7f7b21907869ada7f940f9595efa56063248b4f2669f4cd60696cb84c8a961f055782dde5f3416d3bf2a27a55','Ava Roberts','Vice President','FX Options Trading'),(17,'MT12345','password','OFOI4HLNC3','9b15e0f67f23a55472915250fd6b5c0812264690dec16443596d598d8ca630072d57948cf237440022a0a6293f013968fceec7a29e0b25943bd65caeb35ef00d','Mia Thomas','Associate','Financial Institutions Group'),(18,'JJ12345','password','2YG1JEDGBS','e4fd8999dde5a268d9e1bc4692573d7a1fb3c4d4a6eb245ece868736a688ee4ef34e4f1e701d5d349510b079316cc1814c14fc9ce5051f09d511948adb2dcd29','Jessica Johnson','Director','Rates Quantiative Analysis'),(19,'IS12345','password','0HLR1RMLO3','7ffdb06456a046edce175d242cdf8372d2e8ff8f2e89e3b4a7fb94f83b5b99dc8e802c7dc56db14723b6efb82b285ed7e19da3bfc444e01a6cbba913958d4f0c','Isla Smith','Managing Director','Equity Swaps Delta One'),(20,'AW12345','password','PWMT788W8D','d094f0a1c764660a7ad12c01e88728a2f13a4861b5d1eb3a36fb287ec1f0339961c8f4833c0f3381218eb4f53d328de115777f191213ebd4145e7e551cf4efa7','Amelia Williams','Director','FX Quantitative Developer');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-08-06 23:26:53
