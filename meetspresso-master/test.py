  
from __future__ import print_function
from apiclient import discovery
from httplib2 import Http
from oauth2client import file, client, tools

SCOPES = 'https://www.googleapis.com/auth/calendar'
store = file.Storage('storage.json')
creds = store.get()
if not creds or creds.invalid:
    flow = client.flow_from_clientsecrets('credentials.json', SCOPES)
    creds = tools.run_flow(flow, store)
GCAL = discovery.build('calendar', 'v3', http=creds.authorize(Http()))

GMT_OFF = '+03:00'      #Bucharest timezone i think?
EVENT = {
    'summary': 'Scrum',
    'description': 'What we achieved and want we want to work on further: probs cry like',
    'start':  {'dateTime': '2020-08-04T19:00:00%s' % GMT_OFF},
    'end':    {'dateTime': '2020-08-04T22:00:00%s' % GMT_OFF},
    'attendees': [
        {'email': 'madalina.dumitrescu2805@gmail.com'},
        {'email': 'jonathankytang@gmail.com'},
    ],
}

e = GCAL.events().insert(calendarId='primary',
        sendNotifications=True, body=EVENT).execute()

print('''*** %r event added:
    Start: %s
    End:   %s''' % (e['summary'].encode('utf-8'),
        e['start']['dateTime'], e['end']['dateTime']))