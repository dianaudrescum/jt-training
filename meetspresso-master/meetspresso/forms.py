from flask_wtf import FlaskForm

from wtforms import StringField
from wtforms import HiddenField
from wtforms import PasswordField
from wtforms import SubmitField
from wtforms import DateField
from wtforms import TimeField
from wtforms import validators

from meetspresso.models import Users

class loginForm(FlaskForm):
    soeid = StringField('soeid', validators = [validators.DataRequired()])
    password = PasswordField('password', validators =[validators.DataRequired()])
    submit = SubmitField('submit', [validators.DataRequired()])

class registerForm(FlaskForm):
    soeid = StringField('username', validators = [validators.DataRequired()])
    password = PasswordField('password', validators =[validators.DataRequired()])
    name = StringField('name', validators =[validators.DataRequired()])
    title = StringField('title', validators =[validators.DataRequired()])
    team = StringField('team', validators =[validators.DataRequired()])
    submit = SubmitField('submit', [validators.DataRequired()])

class randomAppointmentForm(FlaskForm):
    date = DateField('startdate', validators = [validators.DataRequired()])
    startTime = TimeField('starttime', validators = [validators.DataRequired()])
    endTime = TimeField('endtime', validators = [validators.DataRequired()])
    location = StringField('location', validators = [validators.DataRequired()])
    submit = SubmitField('submit', [validators.DataRequired()])
	