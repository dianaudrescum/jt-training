import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_mail import Mail

app = Flask(__name__)
app.config["CACHE_TYPE"] = "null"

# configure the sqlalchenmy database URI that is used for the connection
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:password@localhost/meetspresso'
app.config['SQLALCHEMY_ECHO'] = False

# initialise the sqlalchemy database connection
db = SQLAlchemy()
db.init_app(app)

# initialise flask-login
login_manager = LoginManager()
login_manager.init_app(app)

app.secret_key = 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT'

from meetspresso import routes
