class User:

    def __init__(self, userid,username):
        self.userid = userid
        self.username = username

    @property
    def username(self):
        return self.username

    @property
    def is_authenticated(self):
        return True

    @property
    def is_active(self):
        return True

    @property
    def is_anonymous(self):
        return False

    def get_id(self):
       return self.userid 

    def __repr__(self):
        return 'userid {}'.format(self.userid) 

            
