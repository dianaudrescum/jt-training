from flask_sqlalchemy import SQLAlchemy
import datetime
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from meetspresso import app, db

class Users(db.Model):
    userid = db.Column(db.Integer, primary_key = True)
    soeid = db.Column(db.String(7))
    password = db.Column(db.String(45))
    salt = db.Column(db.String(45))
    hashed = db.Column(db.String(100))
    name = db.Column(db.String(45))
    title = db.Column(db.String(45))
    team = db.Column(db.String(45))

    def get_id(self):
        return self.userid

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

class Appointments(db.Model):
    appointmentid = db.Column(db.Integer, primary_key = True)
    userid1 = db.Column(db.Integer, db.ForeignKey('users.userid'))
    userid2 = db.Column(db.Integer, db.ForeignKey('users.userid'))
    startdate = db.Column(db.DateTime)
    enddate = db.Column(db.DateTime)
    timezone = db.Column(db.String(10))
    location = db.Column(db.String(50))
    status = db.Column(db.String(20))


class Random(db.Model):
    randomid = db.Column(db.Integer, primary_key = True)
    userid = db.Column(db.Integer, db.ForeignKey('users.userid'))

