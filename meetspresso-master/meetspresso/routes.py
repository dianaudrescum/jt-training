import os
import random
import string
import json
import requests
from datetime import datetime

from flask import request
from flask import render_template
from flask import redirect, url_for
from flask import session, flash, abort
from flask import jsonify
from flask import url_for

from flask_login import login_required
from flask_login import login_user, logout_user, current_user

from flask_mail import Message

from meetspresso import app, db, login_manager
from meetspresso.forms import loginForm, registerForm
from meetspresso.forms import randomAppointmentForm
from meetspresso.models import db,Users,Appointments,Random
from meetspresso.passwordhelper import PasswordHelper
from meetspresso.events import Google_cal_events


# instantiate a password helper object
ph = PasswordHelper()

class MyEncoder(json.JSONEncoder):
    def default(self, o):
        return o.__dict__


#---- the callback function for flask-login
@login_manager.user_loader
def load_user(userid):
    return Users.query.get(int(userid))

@app.errorhandler(404)
def page_not_found(error):
   return render_template('404.html', title = '404'), 404

@app.route('/', methods = ['GET', 'POST'])
def index():
    if current_user.is_authenticated:
        return redirect('menu')
    form = loginForm()
    if form.validate_on_submit():
        password = form.password.data
        soeid = form.soeid.data
        user = Users.query.filter_by(soeid=soeid).first()
        # check if user exists and validate the password with the salt
        # if user and password:
        if user and ph.validate_password(password,user.salt,user.hashed):
            login_user(user)
            return redirect(url_for('menu'))
        else:
            flash('please try again')
    return render_template('login.html',form=form)


@app.route('/menu', methods = ['GET', 'POST'])
def menu():
    if not current_user.is_authenticated:
        return redirect(url_for('index'))

    options = ["random", "team", "favourites"]

    return render_template("menu.html", options=options)


@app.route('/schedule', methods = ['GET', 'POST'])
def schedule():
    userid = current_user.userid
    appts = Appointments.query.filter((Appointments.userid1==userid) | (Appointments.userid2==userid))
    cleaned_appts = []
    
    for i in appts:
        name1 = Users.query.get(int(i.userid1)).name
        if name1 == current_user.name:
            name = Users.query.get(int(i.userid2)).name
        else:
            name = Users.query.get(int(i.userid1)).name

        appt = {
            "appointmentid": i.appointmentid,
            "userid1": i.userid1,
            "userid2": i.userid2, 
            "name": name,
            "location": i.location,
            "date": i.startdate.date().strftime('%d %b %Y'),
            "starttime": i.startdate.time().strftime('%H:%M'),
            "endtime": i.enddate.time().strftime('%H:%M'),
            "status": i.status
        }

        cleaned_appts.append(appt)
        
    return render_template("schedule.html", appointments=cleaned_appts)


@app.route('/appointment/accept', methods = ['GET', 'POST'])
def appointment_accept():
    if request.args.get("id"):
        # Update appointment record in database to accept
        appointmentid = request.args.get("id")
        status = "accepted"
        appt = Appointments.query.filter_by(appointmentid=appointmentid).first()
        appt.status = status
        db.session.commit()

        # Add to Google Calendar
        name = Users.query.filter_by(userid=appt.userid2).first().name
        starttime = appt.startdate.isoformat()
        endtime = appt.enddate.isoformat()
        events = Google_cal_events.create_event(name, starttime, endtime)
        return redirect(url_for("schedule"))
    return redirect(url_for("menu"))


@app.route('/appointment/decline', methods = ['GET', 'POST'])
def appointment_decline():
    if request.args.get("id"):
        appointmentid = request.args.get("id")
        status = "declined"
        appt = Appointments.query.filter_by(appointmentid=appointmentid).first()
        appt.status = status
        db.session.commit()
        return redirect(url_for("schedule"))
    return redirect(url_for("menu"))


def get_random_person():
    random_id = db.engine.execute('SELECT randomid FROM random ORDER BY randomid DESC LIMIT 1')
    random_id = int(str(random_id.fetchall()[0])[1])
    if(random_id != 0):
        random_id += 1
    print(random_id)
    user_id = current_user.userid
    query = 'SELECT userid FROM random WHERE userid={0}'.format(user_id)
    checkUser = db.engine.execute(query)
    # # if checkUsername != 0:
    # #     print('User already in random')
    random_user = str([db.engine.execute('SELECT userid FROM random LIMIT 1').fetchall()[0]][0])[:-2][1:]
    delete = db.engine.execute('DELETE FROM random LIMIT 1')
    query2 = "INSERT INTO random(randomid, userid) VALUES ({0}, {1})".format(random_id, user_id)
    add_user = db.engine.execute(query2)
    return random_user

    # return Users.query.filter_by(soeid="JT49863").first()


@app.route('/appointment/random', methods = ['GET', 'POST'])
def appointment_random():
    events = Google_cal_events.top_ten()
    cleaned_events = []
    for event in events:

        e = {
            "summary" : event["summary"],
            "start_date" : event["start"]["dateTime"].split("T")[0],
            "start_time" : event["start"]["dateTime"].split("T")[1].split("+")[0:5][0][:-3],
            "end_time" : event["end"]["dateTime"].split("T")[1].split("+")[0:5][0][:-3]
        }

        cleaned_events.append(e)

    paired_user = get_random_person()

    paired_user = Users.query.filter_by(userid=15).first()

    form = randomAppointmentForm()
    if form.validate_on_submit():
        print(form.date.data)
        userid1 = current_user.userid
        userid2 = paired_user.userid
        date = form.date.data
        starttime = form.startTime.data
        endtime = form.endTime.data
        location = form.location.data
        status = "pending"
        cleaned_start_date = datetime.combine(date, starttime)
        cleaned_end_date = datetime.combine(date, endtime)

        new_appt = Appointments(userid1=userid1,
                                userid2=userid2,
                                startdate=cleaned_start_date,
                                enddate=cleaned_end_date,
                                timezone="",
                                location=location,
                                status=status
                                )

        db.session.add(new_appt)
        db.session.commit()
        flash("Coffee invite with " + paired_user.name + " sent!")
        return redirect(url_for("schedule"))
    return render_template("random.html", form=form, events=cleaned_events)


@app.route('/login', methods = ['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect('menu')
    form = loginForm()
    if form.validate_on_submit():
        password = form.password.data
        soeid = form.soeid.data
        user = Users.query.filter_by(soeid=soeid).first()
        # check if user exists and validate the password with the salt
        # if user and password:
        if user and ph.validate_password(password,user.salt,user.hashed):
            login_user(user)
            return redirect(url_for('index'))
        else:
            flash('please try again')
    return render_template('login.html',form=form)


@app.route('/register', methods = ['GET', 'POST'])
def register():
    # check if user is already logged in
    if current_user.is_authenticated:
        return redirect(url_for('menu'))

    form = registerForm()

    if form.validate_on_submit():
        soeid = form.soeid.data
        password = form.password.data
        name = form.name.data
        title = form.title.data
        team = form.team.data
        # generate a salt of 10 alphanumeric random characters
        random_salt = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(10))
        # add the salt to the password
        password = random_salt + password
        # hash the password and encode it
        password_hashed = ph.get_hash(password.encode('utf-8'))

        # create object and add it to the users table
        user = Users(soeid=form.soeid.data, 
                    password=form.password.data, 
                    name=form.name.data,
                    title=form.title.data,
                    team=form.team.data,
                    hashed=password_hashed, 
                    salt=random_salt)
        db.session.add(user)
        db.session.commit()
        flash("account created", "success")
        return redirect(url_for('login'))

    return render_template('register.html', form=form)


@app.route('/logout')
def logout():
    # remove the username from the session if it's there
    if current_user.is_authenticated:
        logout_user()
        flash("You have logged out", "success")
    return redirect(url_for('index'))


@app.route('/account', methods=['GET', 'POST'])
def account():
    if current_user.is_authenticated:
        # get the current logged in user id
        userid = current_user.userid
        user = Users.query.filter_by(userid=userid).first()
        return render_template('account.html', user=user)
    return redirect(url_for('index'))
